<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/getEbayOfficialTime',function(){
	EbayTrading::getEbayOfficialTime();
})->name('getEbayOfficialTime');

Route::get('/getMainCategories',function(){
	EbayTrading::getMainCategories();
})->name('getMainCategories');

Route::get('/getStoreDetails',function(){
	EbayTrading::getStoreDetails();
})->name('getStoreDetails');

Route::get('/addFixedPriceItem',function(){
	EbayTrading::addFixedPriceItem();
})->name('addFixedPriceItem');
