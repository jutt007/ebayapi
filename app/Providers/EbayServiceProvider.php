<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Api\EbayTrading;

class EbayServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('EbayTrading', function () {
            return new EbayTrading;
        });
    }
}