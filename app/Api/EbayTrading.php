<?php

namespace App\Api;

class EbayTrading
{
    /*
    |--------------------------------------------------------------------------
    | Ebay Trading Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling ebay trading api calls.
    |
    */

    protected $app_id;
    protected $endpoint;
    protected $dev_id;
    protected $cert_id;
    protected $token;
    protected $ru_name;
    protected $headers;

    /**
     * constructor for class
     *
     * @return void
     */
    public function __construct()
    {
        $this->app_id = env('EBAY_APP_ID');
        $this->dev_id = env('EBAY_DEV_ID');
        $this->cert_id = env('EBAY_CERT_ID');
        $this->ru_name = env('EBAY_RU_NAME');
        $this->token = env('EBAY_TOKEN');

        $this->endpoint = "https://api.sandbox.ebay.com/ws/api.dll";

        $this->headers = array(
                    'Content-Type: text/xml',
                    'X-EBAY-API-COMPATIBILITY-LEVEL: 971',
                    'X-EBAY-API-DEV-NAME: '.$this->dev_id,
                    'X-EBAY-API-APP-NAME: '.$this->app_id,
                    'X-EBAY-API-CERT-NAME: '.$this->cert_id,
                    'X-EBAY-API-SITEID: 0'
        );
    }

    public function getEbayOfficialTime($call_name = 'X-EBAY-API-CALL-NAME: GeteBayOfficialTime')
    {
        array_push($this->headers, $call_name);

        $xml =  '<?xml version="1.0" encoding="utf-8"?>'.
                '<GeteBayOfficialTimeRequest xmlns="urn:ebay:apis:eBLBaseComponents">'.
                '<RequesterCredentials>'.
                '<eBayAuthToken>'.$this->token.'</eBayAuthToken>'.
                '</RequesterCredentials>'.
                '<ErrorLanguage>en_US</ErrorLanguage>'.
                '<WarningLevel>High</WarningLevel>'.
                '</GeteBayOfficialTimeRequest>';
        
        $result = (array)$this->request($xml);

        if($result['Ack']=="Failure"){
            dd($result);
        }else{
            dd($result);
        }
    }

    public function getMainCategories($call_name = 'X-EBAY-API-CALL-NAME: GetCategories')
    {
        array_push($this->headers, $call_name);

        $xml =  '<?xml version="1.0" encoding="utf-8"?>'.
                '<GetCategoriesRequest xmlns="urn:ebay:apis:eBLBaseComponents">'.
                '<RequesterCredentials>'.
                '<eBayAuthToken>'.$this->token.'</eBayAuthToken>'.
                '</RequesterCredentials>'.
                '<ErrorLanguage>en_US</ErrorLanguage>'.
                '<WarningLevel>High</WarningLevel>'.
                '<DetailLevel>ReturnAll</DetailLevel>'.
                '<LevelLimit>1</LevelLimit>'.
                '<ViewAllNodes>true</ViewAllNodes>'.
                '</GetCategoriesRequest> ';

        $result = (array)$this->request($xml);

        if($result['Ack']=="Failure"){
            dd($result);
        }else{
            dd($result);
        }
    }

    public function getStoreDetails($call_name = 'X-EBAY-API-CALL-NAME: GetStore')
    {
        array_push($this->headers, $call_name);

        $xml =  '<?xml version="1.0" encoding="utf-8"?>'.
                '<GetStoreRequest xmlns="urn:ebay:apis:eBLBaseComponents">'.
                '<RequesterCredentials>'.
                '<eBayAuthToken>'.$this->token.'</eBayAuthToken>'.
                '</RequesterCredentials>'.
                '<ErrorLanguage>en_US</ErrorLanguage>'.
                '<WarningLevel>High</WarningLevel>'.
                '<LevelLimit>1</LevelLimit>'.
                '</GetStoreRequest>';

        $result = (array)$this->request($xml);

        if($result['Ack']=="Failure"){
            dd($result);
        }else{
            dd($result);
        }
    }

    public function addFixedPriceItem($attributes = null,$call_name = 'X-EBAY-API-CALL-NAME: AddFixedPriceItem')
    {
        array_push($this->headers, $call_name);

        $attributes['PayPalEmailAddress'] = 'dotlogics5@gmail.com';
        $attributes['CategoryID'] = 37565;
        $attributes['postalCode'] = 95125;
        $attributes['sku'] = '95ACFG125';
        $attributes['price'] = 100;
        $attributes['title'] = 'New Ralph Lauren Polo shirt Pink Black Blue Yellow';
        $attributes['description'] = 'New Ralph Lauren Polo shirt Pink Black Blue Yellow';
        $attributes['pictures'] = [
                                    'http://i12.ebayimg.com/03/i/04/8a/5f/a1_1_sbl.JPG',
                                    'http://i12.ebayimg.com/03/i/04/8a/5f/a1_1_sbl.JPG',
                                    'http://i12.ebayimg.com/03/i/04/8a/5f/a1_1_sbl.JPG'
                                ];

        $xml =  '<?xml version="1.0" encoding="utf-8"?>'.
                '<AddFixedPriceItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">'.
                '<RequesterCredentials>'.
                '<eBayAuthToken>'.$this->token.'</eBayAuthToken>'.
                '</RequesterCredentials>'.
                '<ErrorLanguage>en_US</ErrorLanguage>'.
                '<WarningLevel>High</WarningLevel>'.
                '<Item>'.
                '<Country>US</Country>'.
                '<Currency>USD</Currency>'.
                '<DispatchTimeMax>3</DispatchTimeMax>'.
                '<ListingDuration>GTC</ListingDuration>'.
                '<ListingType>FixedPriceItem</ListingType>'.
                '<PaymentMethods>PayPal</PaymentMethods>';

                if(isset($attributes['PayPalEmailAddress'])){
                    $xml .= '<PayPalEmailAddress>'.$attributes['PayPalEmailAddress'].'</PayPalEmailAddress>';
                }

                if(isset($attributes['CategoryID'])){
                    $xml .= '<PrimaryCategory><CategoryID>'.$attributes['CategoryID'].'</CategoryID></PrimaryCategory>';
                }

                if(isset($attributes['postalCode'])){
                    $xml .= '<PostalCode>'.$attributes['postalCode'].'</PostalCode>';
                }

                if(isset($attributes['sku'])){
                    $xml .= '<SKU>'.$attributes['sku'].'</SKU>';
                }

                if(isset($attributes['title'])){
                    $xml .= '<Title>'.$attributes['title'].'</Title>';
                }

                if(isset($attributes['description'])){
                    $xml .= '<Description>'.$attributes['description'].'</Description>';
                }

                if(isset($attributes['price'])){
                    $xml .= '<StartPrice>'.$attributes['price'].'</StartPrice>';
                }

                if(isset($attributes['pictures'])){
                    $xml .= '<PictureDetails>';

                    foreach ($attributes['pictures'] as $image) {
                        $xml .= '<PictureURL>'.$image.'</PictureURL>';
                    }

                    $xml .= '</PictureDetails>';
                }

        $xml .= '<ShippingDetails>'.
                '<CalculatedShippingRate>'.
                '<OriginatingPostalCode>95125</OriginatingPostalCode>'.
                '<MeasurementUnit>English</MeasurementUnit>'.
                '<PackageDepth>6</PackageDepth>'.
                '<PackageLength>7</PackageLength>'.
                '<PackageWidth>7</PackageWidth>'.
                '<ShippingPackage>PackageThickEnvelope</ShippingPackage>'.
                '<WeightMajor>2</WeightMajor>'.
                '<WeightMinor>0</WeightMinor>'.
                '</CalculatedShippingRate>'.
                '<PaymentInstructions>Payment must be received within 7 business days of purchase.</PaymentInstructions>'.
                '<SalesTax>'.
                '<SalesTaxPercent>8.75</SalesTaxPercent>'.
                '<SalesTaxState>CA</SalesTaxState>'.
                '</SalesTax>'.
                '<ShippingServiceOptions>'.
                '<FreeShipping>true</FreeShipping>'.
                '<ShippingService>USPSPriority</ShippingService>'.
                '<ShippingServicePriority>1</ShippingServicePriority>'.
                '</ShippingServiceOptions>'.
                '<ShippingType>Calculated</ShippingType>'.
                '</ShippingDetails>';

        $xml .= '</Item>'.
                '</AddFixedPriceItemRequest>';

        $result = (array)$this->request($xml);

        if($result['Ack']=="Failure"){
            dd($result);
        }else{
            dd($result);
        }
    }

    public function request($xml='')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);        
        curl_setopt($ch, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $result = curl_exec($ch);

        curl_close($ch);

        $result = new \SimpleXMLElement($result);

        return $result;
    }
}