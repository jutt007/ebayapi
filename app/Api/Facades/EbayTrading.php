<?php

namespace App\Api\Facades;
use Illuminate\Support\Facades\Facade;

class EbayTrading extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'EbayTrading';
    }
}